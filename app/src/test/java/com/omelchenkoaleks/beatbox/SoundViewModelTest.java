package com.omelchenkoaleks.beatbox;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class SoundViewModelTest {

    private BeatBox mBeatBox;
    private Sound mSound;
    // имя переменной используется такое, что явно показать,
    // что это будет тестируемый объект, а остальные нет
    private SoundViewModel mSubject;

    // внутри этого метода строится экземпляр SoundViewModel для тестирования
    @Before
    public void setUp() throws Exception {
        // метод mock() создает нам фиктивный объект BeatBox,
        // нужный при тестировании SoundViewModel
        mBeatBox = mock(BeatBox.class);

        mSound = new Sound("assetPath");
        mSubject = new SoundViewModel(mBeatBox);
        mSubject.setSound(mSound);
    }

    // тестируем свойство title
    @Test
    public void exposesSoundNameAsTitle() {
        assertThat(mSubject.getTitle(), is(mSound.getName()));
    }

    @Test
    public void callsBeatBoxPlayOnButtonClicked() {
        mSubject.onButtonClicked();

        // это означает, что метод play(…) был вызван для mBeatBox с передачей mSound в параметре
        verify(mBeatBox).play(mSound);
    }
}