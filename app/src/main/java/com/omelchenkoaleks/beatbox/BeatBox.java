package com.omelchenkoaleks.beatbox;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BeatBox {

    // для вывода информации в журнал
    private static final String TAG = "BeatBox";

    // имя папки в которую будут сохранены звуки
    private static final String SOUNDS_FOLDER = "sample_sounds";

    // будет хранить возможное количество одновременно проигрываемых звуков
    // в объекте SoundPool
    private static final int MAX_SOUNDS = 5;

    // для обращения к активам используется класс AssetManager
    private AssetManager mAssets;

    // будет хранить список объектов Sound
    private List<Sound> mSounds = new ArrayList<>();

    // класс SoundPool позволяет загрузить в память большое количество звуков
    // и проигрывать их одновременно
    private SoundPool mSoundPool;

    // BeatBox понадобится экземпляр AssetManager - поэтому предоставим
    // ему Context, который извлечет AssetManager и сохранит его на бущущее
    public BeatBox(Context context) {
        mAssets = context.getAssets();
        // этот конструктор считается устаревшим,
        // но он нужен для обеспечения совместимости
        mSoundPool = new SoundPool(MAX_SOUNDS, AudioManager.STREAM_MUSIC, 0);
        // получаем активы
        loadSounds();
    }

    // проверяем что звук загрузился (!null) и воспроизводим
    public void play(Sound sound) {
        Integer soundId = sound.getSoundId();
        if (soundId == null) {
            return;
        }
        mSoundPool.play(soundId, 1.0f, 1.0f, 1, 0, 1.0f);
    }

    // приложение должно освободить ресурсы SoundPool при завершении работы
    public void release() {
        mSoundPool.release();
    }

    // метод обращается к активам при помощи метод list(String)
    private void loadSounds() {
        String[] soundNames;
        try {
            soundNames = mAssets.list(SOUNDS_FOLDER);
            Log.i(TAG, "Found" + soundNames.length + " sounds");
        } catch (IOException ioe) {
            Log.e(TAG, "Could not list assets", ioe);
            return;
        }

        // строим список объектов Sound
        for (String filename : soundNames) {
            try {
                String assetPath = SOUNDS_FOLDER + "/" + filename;
                Sound sound = new Sound(assetPath);
                // загружаем звуки
                load(sound);
                mSounds.add(sound);
            } catch (IOException ioe) {
                Log.e(TAG, "Could not load sound " + filename, ioe);
            }
        }
    }

    private void load(Sound sound) throws IOException {
        AssetFileDescriptor assetFileDescriptor = mAssets.openFd(sound.getAssetPath());
        // вызов этого метода загружает файл в SoundPool для последующего воспроизведения
        int soundId = mSoundPool.load(assetFileDescriptor, 1);
        sound.setSoundId(soundId);
    }

    public List<Sound> getSounds() {
        return mSounds;
    }
}
