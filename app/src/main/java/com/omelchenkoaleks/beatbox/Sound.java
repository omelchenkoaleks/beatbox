package com.omelchenkoaleks.beatbox;

// нужен объект для хранения имени файла и всей информации,
// которая относится к звуку
public class Sound {

    private String mAssetPath;
    private String mName;
    // какждому загружаемому звуку назначется собственный id
    private Integer mSoundId;

    public Sound(String assetPath) {
        mAssetPath = assetPath;
        String[] components = assetPath.split("/");
        String filename = components[components.length - 1];
        mName = filename.replace(".wav", "");
    }

    public String getAssetPath() {
        return mAssetPath;
    }

    public String getName() {
        return mName;
    }

    // тип Integer, чтобы можно было задать неопределенное состояние
    public Integer getSoundId() {
        return mSoundId;
    }

    public void setSoundId(Integer soundId) {
        mSoundId = soundId;
    }
}
